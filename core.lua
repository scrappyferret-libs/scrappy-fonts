--- Required libraries.

-- Localised functions.
local getFontNames = native.getFontNames
local find = string.find

-- Localised values.

--- Class creation.
local library = {}

-- Table to store all registered fonts
library._fonts = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

end

--- Register a font.
-- @param name The name of the font.
-- @param filename The filename of the font.
-- @return The font.
function library:register( name, filename )

	-- Add the font
	self._fonts[ name ] = filename

	-- And return it
	return self._fonts[ name ]

end

--- Gets a registered font.
-- @param name The name of the font.
-- @return The font, or the passed in name if none found.
function library:get( name )

	if name and system.getInfo( "platform" ) == "nx64" and not find( name, "--switch" ) then
		if self._fonts[ name .. "-switch" ] then
			name = name .. "-switch"
		end
	end

	return self._fonts[ name ] or name

end

--- Registers all the system native fonts.
function library:registerNativeFonts()

	-- Get all the native fonts
	local fonts = getFontNames()

	-- Loop through them all
	for i = 1, #fonts, 1 do

		-- Register them
		self:register( fonts[ i ], fonts[ i ] )

	end

	-- Register the system font
	self.systemFont = native.systemFont
	self:register( "systemFont", native.systemFont )

	-- Register the bold system font
	self.systemFontBold = native.systemFontBold
	self:register( "systemFontBold", native.systemFontBold )

end

--- Gets a list of the names of all registered fonts.
-- @return The list.
function library:list()

	-- Table to store names
	local names = {}

	-- Loop through the fonts
	for k, _ in pairs( self._fonts ) do

		-- Store the name
		names[ #names + 1 ] = k

	end

	-- Return the list of names
	return names

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Fonts library
if not Scrappy.Fonts then

	-- Then store the library out
	Scrappy.Fonts = library

end

-- Return the new library
return library
